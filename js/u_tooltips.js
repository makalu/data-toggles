const tooltip = (item, position, title, content) => {
  item.parentNode.classList.add('ds-tooltip-open')
  item.parentNode.insertAdjacentHTML('afterBegin', '' +
    '<div class="ds-tooltip ds-tooltip-' + position + '">' +
    '<div class="ds-tooltip__title">' + title + '</div>' +
    '<div class="ds-tooltip__content">' + content + '</div>' +
    '</div>');
}

const tooltipRemove = (item) => {
  item.parentNode.querySelector('.ds-tooltip').remove();
}

export {
  tooltip, tooltipRemove
}

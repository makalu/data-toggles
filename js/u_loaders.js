const body = document.querySelector('body');
let
  loaderHtml = '<div class="loader is-shown js-loader"><div class="loader-spinner"></div></div>',
  loaderItemHtml = '<div class="loader loader-widget is-shown js-loader-widget"><div class="loader-spinner"></div></div>',
  loaderDataHtml = '<div class="loader loader-processing js-loader-data is-shown">\n' +
    '    <div class="loader-processing__text">Processing...</div>\n' +
    '    <div class="loader loader-widget is-shown">\n' +
    '        <div class="loader-spinner"></div>\n' +
    '    </div>\n' +
    '</div>\n',
  loaderNode = ('.js-loader'),
  loaderItemNode = ('.js-loader-widget'),
  loaderDataNode = ('.js-loader-data')

// Loader on Page
const d_loader = () => {
  body.classList.add('ds-loader-active')
  body.insertAdjacentHTML('afterBegin', loaderHtml);
}

const d_loaderClose = () => {
  body.classList.remove('ds-loader-active')
  if (document.querySelector(loaderNode)) {
    document.querySelector(loaderNode).remove()
  }
}

// Loaders on Item
const d_loaderItem = (item, itemClass) => {
  item.parentNode.classList.add('ds-loader-active')
  item.parentNode.classList.add(itemClass)
  item.parentNode.insertAdjacentHTML('afterBegin', loaderItemHtml);
}

const d_loaderItemClose = (item, itemClass) => {
  item.parentNode.classList.remove('ds-loader-active', 'pos-rel')
  item.parentNode.classList.remove(itemClass)
  if (document.querySelector(loaderItemNode)) {
    item.parentNode.querySelector(loaderItemNode).remove();
  }
}

// Loaders on Target
const d_loaderTarget = (target, targetClass) => {
  target.classList.add('ds-loader-active')
  target.classList.add(targetClass)
  target.insertAdjacentHTML('afterBegin', loaderItemHtml);
}

const d_loaderTargetClose = (target, targetClass) => {
  target.parentNode.classList.remove('ds-loader-active')
  target.parentNode.classList.remove(targetClass)
  if (target.parentNode.querySelector(loaderItemNode)) {
    target.parentNode.querySelector(loaderItemNode).remove();
  }
}

// Loader for Data processing (ajax calls, etc.)
const d_loaderData = (data) => {
  data.insertAdjacentHTML('afterBegin', loaderDataHtml);
}

const d_loaderDataClose = (data) => {
  data.parentNode.classList.remove('ds-loader-active')
  if (document.querySelector(loaderDataNode)) {
    data.parentNode.querySelector(loaderDataNode).remove();
  }
}

const d_siteLoader = () => {
  const loader = document.querySelector(".js-loader");

  window.addEventListener("DOMContentLoaded", function (event) {
    setTimeout(function () {
      loader.remove();
    }, 1500);
  });
}

export {
  d_siteLoader,
  d_loader,
  d_loaderClose,
  d_loaderItem,
  d_loaderItemClose,
  d_loaderTarget,
  d_loaderTargetClose,
  d_loaderData,
  d_loaderDataClose
}

# README or don't #

* Version 1.1

### Toggle Actions ###

<pre>[data-toggle="item"]</pre>

* **ITEM** trigger selector.

<pre>[data-toggle="tooltip"]</pre>

* Trigger for the **TOOLTIP**.

### Actions ###

<pre>[data-target="selector"]</pre>

* **TARGET** container selector.
* It can be **CLASS** or **ID**.
* By default, target gets **'is-active'** class.

<pre>[data-toggle-class="class"]</pre>

* Toggles any **CLASS** on **ITEM**.

<pre>[data-close="close-outside"]</pre>

* Closes **TARGET** on any click outside the **ITEM**.

<pre>[data-close="item"]</pre>

* Closes **PARENT** container.

<pre>[data-close="target"]</pre>

* Closes **TARGET** container (to do).

### Overlays ###

<pre>[data-overlay="body"]</pre>

* Opens an **OVERLAY** on a **BODY** on pseudo ::after element.

<pre>[data-overlay="item"]</pre>

* Opens an **OVERLAY** on a **ITEM** element.

<pre>[data-overlay="target"]</pre>

* Opens an **OVERLAY** on a **TARGET** element.

### Loaders ###

<pre>[data-loader="page"]</pre>

* Triggers a page **LOADER**.

<pre>[data-loader="item"]</pre>

* Triggers a **LOADER** on a **ITEM** element.

<pre>[data-loader="target"]</pre>

* Triggers a **LOADER** on a **TARGET** element.

<pre>[data-loader="data"]</pre>

* Triggers a data processing **LOADER**.

### Effects ###

<pre>[data-effect="block"]</pre>

* Default **display none/block** toggle.

<pre>[data-effect="slide-left"]</pre>

* Slides **BOX** from the **left**.

<pre>data-effect="slide-right"]</pre>

* Slides **BOX** from the **right**.

<pre>[data-effect="slide-top"]</pre>

* Slides **BOX** from the **top**.

<pre>[data-effect="slide-bottom"]</pre>

* Slides **BOX** from the **bottom**.

<pre>data-effect="slide-modal""</pre>

* Opens **MODAL** with or without overlay.

### Timer ###

<pre>data-timer="milliseconds""</pre>

* Opens **TARGET** after time delay.

### Tooltips ###

<pre>[data-tooltip="top"]</pre>

* Position of a **tooltip**.
* Available positions: **left**, **right**, **top**, **bottom**.

<pre>[data-tooltip="click"]</pre>

* Triggers tooltip on **CLICK**.

<pre>[data-tooltip="hover"]</pre>

* Triggers tooltip on **HOVER**.

<pre>[data-title="title"]</pre>

* Tooltip **TITLE**.

<pre>[data-content="any text or <span>html</span>"]</pre>

* Tooltip **CONTENT**.
* Can be TEXT or HTML.

### Snippets ###

<pre>[data-visible="mobile"]</pre>

* Visible only on **MOBILE**.

<pre>[data-visible="desktop"]</pre>

* Visible only on **DESKTOP**.

### Text Reveal Effects ###

<pre>[data-toggle="reveal"]</pre>

* Triggers animation on **TEXT REVEAL** boxes when they enter viewport.

<pre>[data-repeat="true"]</pre>

* Repeat animation on each enter.

<pre>[data-reveal="name-of-effect"]</pre>

* Name of effect in SCSS file.

### In Viewport Effects ###

<pre>[data-trigger="viewport"]</pre>

* Trigger class on **BOX** when it enters viewport.

<pre>[data-repeat="true"]</pre>

* Repeat class toggle on each enter.

<pre>[data-reveal="name-of-effect"]</pre>

* Name of effect in SCSS file.

TODO: Mobile/desktop query of each part.

### SCSS ###

Project is using [include-media.scss](https://eduardoboucas.github.io/include-media/) and some random mixins and functions.

All effects are using SCSS for toggling.

All effect styling is done in _effects.scss

All item styling is done in _item.scss

_demo.scss is for demo purposes.

Build on [parcel.js](https://parceljs.org/)

### Who do I talk to? ###

[voodoosnares@gmail.com](mailto:voodoosnares@gmail.com)

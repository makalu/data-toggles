/**
 * Visibility functions
 */

import { overlayRemove } from "./u_fn";

const u_showDisplay = (elem) => {
  elem.style.display = "block";
}

const u_hideDisplay = (elem) => {
  elem.style.display = "none";
}

const u_showElem = (elem, hidden = 'is-hidden', visible = 'is-active') => {
  elem.classList.remove(hidden);
  elem.classList.add(visible);
}

const u_hideElem = (elem, hidden = 'is-hidden', visible = 'is-active') => {
  elem.classList.add(hidden);
  elem.classList.remove(visible);
}

const u_toggleElem = (elem, hidden = 'is-hidden') => {
  elem.classList.toggle(hidden);
}

// Show Function
const show = (target, item) => {
  u_showElem(target);
  target.dataset.visible = 'true'

  if (item.dataset.toggleClass) {
    let classToggle = item.dataset.toggleClass
    item.classList.add(classToggle)
  }

  if (item.ariaExpanded) {
    item.ariaExpanded = 'true'
  }
}

// Hide Function
const hide = (target, item) => {
  u_hideElem(target);
  target.dataset.visible = 'false'
  if (item.dataset.toggleClass) {
    let classToggle = item.dataset.toggleClass
    item.classList.remove(classToggle)
  }
  if (item.ariaExpanded) {
    item.ariaExpanded = 'false'
  }
}

export {
  u_showElem,
  u_hideElem,
  u_toggleElem,
  u_showDisplay,
  u_hideDisplay,
  hide,
  show
}

function isInViewport() {
  const isIO = (element, repeat, offset) => {
    const observer = new window.IntersectionObserver(([entry]) => {
      // console.log(entry.boundingClientRect.top)
      if (entry.isIntersecting) {
        element.classList.add('is-active')
        // console.log(entry.boundingClientRect.top)
        // if (entry.boundingClientRect.top > offset) {
        //     element.classList.add('is-active')
        // } else {
        //     element.classList.remove('is-active')
        // }
        observer.unobserve(entry.target);

      } else {
        if (repeat === 'true') {
          element.classList.remove('is-active')
        }
      }

    }, {
      root: null,
      // rootMargin: null,
      threshold: 1,
    })
    observer.observe(element)

    return { IntersectionObserver };
  }

  const isInView = (element, repeat) => {
    const rect = element.getBoundingClientRect();
    if (repeat === 'true' ) {
      element.classList.remove('is-active')
    }
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  return { isIO, isInView }
}


export { isInViewport }

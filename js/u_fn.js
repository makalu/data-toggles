// Item Closing
// Close on click outside of the container
import { hide } from "./u_show-hide-display";

const body = document.querySelector('body');
let
  overlayHtml = '<div class="ds-overlay js-overlay"></div>',
  overlayNode = ('.js-overlay'),
  loaderNode = ('.js-loader'),
  loaderItemNode = ('.js-loader-widget'),
  loaderDataNode = ('.js-loader-data');

const dataCloseClickOutside = (target, item, overlayLocation) => {
  document.addEventListener('click', function (e) {
    e.preventDefault();
    let isClickInside = item.contains(e.target);
    if (!isClickInside) {
      hide(target, item)

      // Remove overlay (if exists)
      if (overlayLocation) {
        overlayRemove(overlayLocation)
      }
    }
  });
}

// Close parent item
const dataCloseParentItem = (target, item, overlayLocation) => {
  target = item.closest('[data-visible]')

  if (!target) return;
  target.dataset.visible = 'false';
  target.classList.remove('is-active');
  target.classList.add('is-hidden');

  // Remove overlay (if exists)
  if (overlayLocation) {
    overlayRemove(overlayLocation)
  }

  // Toggle Aria-Expanded of Trigger attribute (if exists)
  let togglerName = target.id;
  let togglerItem = document.querySelectorAll('[data-target="#' + togglerName + '"]')

  for (let i of togglerItem) {
    if (i.ariaExpanded === 'true') {
      i.setAttribute('aria-expanded', 'false');
    }
  }

  // Remove loader on Target (if exists)
  if (target.classList.contains('ds-loader-active')) {
    target.querySelector(loaderItemNode).remove();
    target.classList.remove('ds-loader-active')
  }
}

// Toggle / Close Overlays
const overlayActive = (overlayLocation, overlayClass) => {
  // if (!overlayLocation.classList.contains('ds-overlay-active')) {
  overlayLocation.insertAdjacentHTML('afterBegin', overlayHtml);
  overlayLocation.classList.add(overlayClass)
  if (overlayClass) {
    overlayLocation.classList.add(overlayClass);
  }
  // }
}

const overlayRemove = (overlayLocation, overlayClass) => {
  // if (overlayLocation.classList.contains('ds-overlay-active')) {
  overlayLocation.classList.remove(overlayClass);
  if (overlayLocation.querySelector(overlayNode)) {
    overlayLocation.querySelector(overlayNode).remove();
  }
  // }
}

export {
  dataCloseClickOutside, dataCloseParentItem, overlayActive, overlayRemove
}

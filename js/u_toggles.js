import { show, hide } from "./u_show-hide-display";
import {
  d_loader,
  d_loaderClose,
  d_loaderItem,
  d_loaderItemClose,
  d_loaderTarget,
  d_loaderTargetClose,
  d_loaderData,
  d_loaderDataClose,
} from "./u_loaders";
import { tooltip, tooltipRemove } from "./u_tooltips";
import { isInViewport } from "./u_viewport";
import { dataCloseClickOutside, dataCloseParentItem, overlayActive, overlayRemove } from "./u_fn";
import { u_throttled } from "./u_trottle_debounce";

class Toggle {
  constructor() {
    this.config = {
      body: document.querySelector('body'),
      trigger: document.querySelectorAll('[data-toggle="item"]'),
      triggerTooltip: document.querySelectorAll('[data-toggle="tooltip"]'),
      reveal: document.querySelectorAll('[data-trigger="reveal"]'),
      viewport: document.querySelectorAll('[data-trigger="viewport"]')
    }
  }

  // Toggles
  toggleItems() {
    // if (!(elem && elem.length > 0)) {
    //   elem = [elem];
    // }

    // Toggle Elements, Overlays, Loaders
    this.config.trigger.forEach((item) => {
        // item.attr = undefined;
        function navigate(e) {
          if (e.type === 'click' || e.keyCode === 13) {
            e.preventDefault();
            let target = document.querySelector(item.dataset.target);

            const attr = {
              state: target.dataset.visible,
              itemClose: item.dataset.close,
              overlay: item.dataset.overlay,
              loader: item.dataset.loader,
              timer: item.dataset.timer,
              target: item.dataset.target,
              areaExpanded: item.ariaExpanded,
            }
            if (!target) return;
            if (!attr.state) return console.log(`Missing Data Visible attribute on ${ item.attr.target }`);

            // Init
            if (!attr.timer) {
              attr.state === 'false' ? show(target, item) : hide(target, item);
            }

            // Overlays
            // Overlay on Body
            attr.overlay === 'body' && !this.config.body.classList.contains('ds-overlay-active')
              // attr.overlay === 'body'
              ? overlayActive(this.config.body, 'ds-overlay-active')
              : overlayRemove(this.config.body, 'ds-overlay-active');

            // Overlay on Item parent
            attr.overlay === 'item' && !item.parentNode.classList.contains('ds-overlay-active')
              // attr.overlay === 'item'
              ? overlayActive(item.parentNode, 'ds-overlay-item')
              : overlayRemove(item.parentNode, 'ds-overlay-item');

            // Overlay on Target
            attr.overlay === 'target' && !target.classList.contains('ds-overlay-active')
              // attr.overlay === 'target'
              ? overlayActive(target, 'ds-overlay-target')
              : overlayRemove(target, 'ds-overlay-target');

            // Loaders
            // Loaders on Body
            attr.loader === 'page' && !this.config.body.classList.contains('ds-loader-active')
              ? d_loader()
              : d_loaderClose();

            // Loaders on Item
            attr.loader === 'item' && !item.parentNode.classList.contains('ds-loader-active')
              ? d_loaderItem(item, 'ds-loader-item')
              : d_loaderItemClose(item, 'ds-loader-item')

            // Loaders on Target
            attr.loader === 'target' && !target.classList.contains('ds-loader-active')
              ? d_loaderTarget(target, 'ds-loader-target')
              : d_loaderTargetClose(target, 'ds-loader-target')

            // Loader for Data processing (ajax calls, etc.)
            attr.loader === 'data' && !target.classList.contains('ds-loader-active')
              ? d_loaderData(target)
              : d_loaderDataClose(target)

            // Item Closing
            // Close parent item
            const closeToggleItems = [...target.querySelectorAll('[data-close="item"]')];

            if (closeToggleItems) {
              closeToggleItems.forEach((item, i) => {
                item.addEventListener('click', () => {
                  dataCloseParentItem(target, item, this.config.body)
                })
              });
            }

            // Close on click outside of the container
            attr.itemClose === 'outside' ? dataCloseClickOutside(target, item, this.config.body) : ''

            // Toggle Target on Timer
            if (attr.timer) {
              let timer = attr.timer;

              function timedShow() {
                attr.state === 'false' ? show(target, item) : hide(target, item);
              }

              setTimeout(timedShow, timer);
            }
          }

          item.addEventListener('click', navigate);
          item.addEventListener('keydown', navigate);
        }
      }
    )
    ;
  }

  // Tooltips
  toggleTooltips() {
    this.config.triggerTooltip.forEach((item) => {
      const attr = {
        title: item.dataset.title,
        content: item.dataset.content,
        trigger: item.dataset.trigger,
        position: item.dataset.tooltip,
      }

      let d_tooltipClick = attr.trigger === 'click'
      let d_tooltipHover = attr.trigger === 'hover'

      if (d_tooltipHover) {
        item.addEventListener('mouseover', () => {
          tooltip(item, attr.position, attr.title, attr.content)
        })

        item.addEventListener('mouseout', () => {
          tooltipRemove(item)
        })
      }

      if (d_tooltipClick) {
        item.addEventListener('click', () => {
          !item.parentNode.querySelector('.ds-tooltip')
            ? tooltip(item, attr.position, attr.title, attr.content)
            : tooltipRemove(item)
        })
      }
    })
  }

  // Text Reveal
  textReveal() {
    // Toggle Elements, Overlays, Loaders
    const inViewReveal = () => {
      this.config.reveal.forEach((item) => {
        const attr = {
          state: item.dataset.visible,
          trigger: item.dataset.trigger,
          revealEffect: item.dataset.reveal,
          repeat: item.dataset.repeat,
        }
        if (!attr.revealEffect) return console.log(`Missing [data-reveal] effect!`);

        if ('IntersectionObserver' in window) {
          isInViewport().isIO(item, attr.repeat === 'true' ? 'true' : '', `"${ attr.offset }"`)
        } else {
          isInViewport().isInView(item, attr.repeat === 'true' ? 'true' : '') ? item.classList.add('is-active') : ''
        }
      });
    }

    const throttleScroll = u_throttled(() => {
      inViewReveal();
    }, 30);

    throttleScroll();
    document.addEventListener('scroll', throttleScroll)
  }

  inViewport() {
    // Toggle class when in viewport
    const inViewElement = () => {
      this.config.viewport.forEach((item) => {
        const attr = {
          repeat: item.dataset.repeat,
          offset: item.dataset.offset
        }

        if ('IntersectionObserver' in window) {
          isInViewport().isIO(item, attr.repeat === 'true' ? 'true' : '', `"${ attr.offset }"`)
        } else {

          isInViewport().isInView(item, attr.repeat === 'true' ? 'true' : '') ? item.classList.add('is-active') : ''
        }
      });
    }

    const throttleScroll = u_throttled(() => {
      inViewElement();
    }, 30);

    throttleScroll();
    document.addEventListener('scroll', throttleScroll)
  }

}

let
  toggle_class = new Toggle();
toggle_class
  .toggleItems();

toggle_class
  .toggleTooltips();

toggle_class
  .textReveal();

toggle_class
  .inViewport();
